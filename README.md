# ReactNative_Breno

Desenvolvido com base no curso de React Native: Desenvolva APPs Nativas para Android e iOS.

Projetos desenvolvidos:
1. Exercíos - Contendo conceitos básicos para a estruturação de uma aplicação React Native.
2. Calculadora - Desenvolvido para exercitar o uso de componentes como botões e display, além da lógica de programação.
3. Campo Minado - Desenvolvido para exercitar o uso de componentes como campo e minas, além da lógica do jogo e JavaScript.
4/5/6. Tasks - Aplicativo para organização pessoal, Desenvolvido para exercitar o uso de componentes de lista, tarefas e datas.
7 - Clone Instagram
8 - Estudo JavaScript



